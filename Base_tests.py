import sys

from selenium import webdriver
import time
import unittest
import warnings
from datetime import datetime

from selenium.webdriver import ActionChains, Keys
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
import re

from Elements import Main_menu_controls, Create_message_form


class Base_tests(unittest.TestCase):

    # Запуск браузера
    def setUp(self):
        service = Service(r"C:\Program Files (x86)\chromedriver.exe")
        options = Options()
        options.binary_location = r"C:\Program Files\Google\Chrome\Application\chrome.exe"  # chrome binary location specified here
        options.add_argument("--start-maximized")  # open Browser in maximized mode
        options.add_argument("--no-sandbox")  # bypass OS security model
        options.add_argument("--disable-dev-shm-usage")  # overcome limited resource problems
        options.add_experimental_option('useAutomationExtension', False)
        warnings.simplefilter('ignore', category=ResourceWarning)

        self.driver = webdriver.Chrome(options=options, service=service)
        self.driver.get("http://igs1.test.ruitb.ru/")
        # self.driver.get("http://158.160.8.77:9999/")
        time.sleep(5)
        password_field = self.driver.find_element(By.XPATH, "//span[contains(@class, 'input')]//input")
        password_field.send_keys("admin")
        password_field.send_keys(Keys.ENTER)
        time.sleep(2)

    # Проверка наличия базовых элементов и разделов меню и их названий
    def test_base_controls(self):
        menu_links = self.driver.find_element(By.XPATH, "//div[@class='app-header']").find_elements(By.XPATH, './/li')
        expected_links = [' Карта', ' Оперативная обстановка', ' Все сообщения']
        # Проверка наличия всех разделов меню и их названий
        for i in menu_links:
            text = i.find_element(By.XPATH, './/span//a').get_attribute('innerHTML')
            self.assertEqual(text, expected_links[expected_links.index(text)])
        try:
            self.driver.find_element(*Main_menu_controls.NAV_BUTTONS).find_elements(By.TAG_NAME, 'button')[0]
            self.driver.find_element(*Main_menu_controls.NAV_BUTTONS).find_elements(By.TAG_NAME, 'button')[1]
            self.driver.find_element(*Main_menu_controls.NAV_BUTTONS).find_elements(By.TAG_NAME, 'button')[2]
            self.driver.find_element(*Main_menu_controls.NAV_BUTTONS).find_elements(By.TAG_NAME, 'button')[3]
            # self.driver.find_element(*Main_menu_controls.NAV_BUTTONS).find_elements(By.TAG_NAME, 'button')[4]
            self.driver.find_element(By.XPATH, "//div[@class='map-ol-map']")
            # self.driver.find_element(By.XPATH, "//div[@id='map-user-control-mi-layer-manager']//button//img")
            # self.driver.find_element(By.XPATH, "//div[@id='map-user-control-mi-transport-trafic']//button//img")
            self.driver.find_element(By.XPATH, "//div[@id='map-user-control-mi-bm']//button//img")
            self.driver.find_element(By.XPATH, "//div[@id='map-user-control-mi-dimensions']//button//img")
            self.driver.find_element(By.XPATH, "//div[@id='map-user-control-mi-print']//button//img")
        except NoSuchElementException:
            print("Проблемы с отображением карты / инструментов карты ")
            raise NoSuchElementException

        # Переход на ОО
        menu_links[1].click()
        time.sleep(2)

        try:
            self.driver.find_element(By.XPATH, "//div[contains(@class,'stopCollapsible stopsForward')]")
            self.driver.find_element(By.XPATH, "//div[contains(@class,'stopCollapsible stopsBackward')]")
            self.driver.find_element(By.XPATH, "//div[@class='routeSituation']")
            self.driver.find_element(By.XPATH, "//div[@class='routeSituationHeader']")
            self.driver.find_element(By.XPATH, "//div[@class='routeSituationBox']")
            plotContainers = self.driver.find_elements(By.XPATH, "//div[@class='plotContainer']")
            self.assertEqual(len(plotContainers), 2)
        except NoSuchElementException:
            print("Проблемы с отображением элементов ОО ")
            raise NoSuchElementException
        except AssertionError:
            print("Неверное кол-во графиков")
            raise AssertionError

        # Переход на события
        menu_links[2].click()
        time.sleep(2)

        try:
            self.driver.find_element(By.XPATH, "//div[contains(@class,'group-critial')]")
            self.driver.find_element(By.XPATH, "//div[contains(@class,'group-warn')]")
            self.driver.find_element(By.XPATH, "//div[contains(@class,'group-info')]")
            self.driver.find_element(By.XPATH, "//section[@class='ant-layout overall-chat-inner']")
        except NoSuchElementException:
            print("Проблемы с отображением элементов раздела События ")
            raise NoSuchElementException

        print("test_base_controls: PASSED!")

    def tearDown(self):
        self.driver.close()


if __name__ == "__main__":
    unittest.main()
