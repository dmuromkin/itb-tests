import sys

import selenium.common.exceptions
from selenium import webdriver
import time
import unittest
import warnings
from datetime import datetime

from selenium.webdriver import ActionChains, Keys
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
import re

from Elements import Main_menu_controls, Create_message_form, Route_situation


class RouteSituation_tests(unittest.TestCase):

    # Запуск браузера
    def setUp(self):
        service = Service(r"C:\Program Files (x86)\chromedriver.exe")
        options = Options()
        options.binary_location = r"C:\Program Files\Google\Chrome\Application\chrome.exe"  # chrome binary location specified here
        options.add_argument("--start-maximized")  # open Browser in maximized mode
        options.add_argument("--no-sandbox")  # bypass OS security model
        options.add_argument("--disable-dev-shm-usage")  # overcome limited resource problems
        options.add_experimental_option('useAutomationExtension', False)
        warnings.simplefilter('ignore', category=ResourceWarning)

        self.driver = webdriver.Chrome(options=options, service=service)
        self.driver.get("http://igs1.test.ruitb.ru/")
        # self.driver.get("http://158.160.8.77:9999/")
        time.sleep(2)
        password_field = self.driver.find_element(By.XPATH, "//span[contains(@class, 'input')]//input")
        password_field.send_keys("admin")
        password_field.send_keys(Keys.ENTER)
        time.sleep(2)

    def test_identification(self):
        route_has_vehicles = False
        # Переход на ОО
        self.driver.find_element(*Main_menu_controls.TRAFFIC_ON_ROUTE_LINK).click()
        time.sleep(4)
        routeContainers = self.driver.find_elements(*Route_situation.ROUTE_CONTAINERS)
        for container in routeContainers:
            vehicles = container.find_elements(By.XPATH, "./*")[0].find_elements(By.TAG_NAME, "svg")
            vehicles_coords = []
            for veh in vehicles:
                x = veh.get_attribute('x')
                if x in vehicles_coords:
                    # raise ValueError('На маршруте есть ТС с одинаковыми координатами.')
                    print(f'На маршруте {routeContainers.index(container)} есть ТС с одинаковыми координатами.')
                    # break
                vehicles_coords.append(x)

            vehicles_titles = container.find_elements(By.XPATH, "./*")[0].find_elements(By.XPATH,
                                                                                        ".//*[contains(@id, 'scatterVehicles-labels')]")
            index = 0
            for veh in vehicles:
                route_has_vehicles = True
                try:
                    if veh.get_attribute("xmlns") is None:
                        veh_number = vehicles_titles[index].find_elements(By.XPATH, "./*")[1].get_attribute('innerHTML')
                        try:
                            veh.click()
                            time.sleep(0.2)
                            circle_color = veh.find_element(By.TAG_NAME, "circle").get_attribute("stroke")
                            self.assertEqual(circle_color, "#39D7FC")
                            card_title = self.driver.find_element(*Route_situation.CARD_TITLE).get_attribute('innerHTML')
                            if len(re.findall(r'\d+', card_title)) > 0:
                                veh_number_in_card = re.findall(r'\d+', card_title)[0]
                                try:
                                    self.assertEqual(veh_number, veh_number_in_card)
                                except AssertionError:
                                    print("Номера ТС не совпали")
                                    print(veh_number_in_card)
                                    print(veh_number)
                                    raise AssertionError
                            # если у ТС не найден путевой лист
                            else:
                                color = veh.get_attribute("fill")
                                self.assertEqual(color, "#000000")
                        except selenium.common.exceptions.ElementClickInterceptedException:
                            print(f"ТС {veh_number} не кликабельно")
                        index += 1
                except selenium.common.exceptions.StaleElementReferenceException:
                    print("ТС вне графа: ")
                    # raise selenium.common.exceptions.StaleElementReferenceException

        try:
            self.assertEqual(route_has_vehicles, True)
            print("test_identification: PASSED")
        except AssertionError:
            print("Нет данных для проверки")
            raise AssertionError

    def test_go_to_map(self):
        # Переход на ОО
        self.driver.find_element(*Main_menu_controls.TRAFFIC_ON_ROUTE_LINK).click()
        time.sleep(4)

        routeContainers = self.driver.find_elements(*Route_situation.ROUTE_CONTAINERS)
        for container in routeContainers:
            vehicles = container.find_elements(By.XPATH, "./*")[0].find_elements(By.TAG_NAME, "svg")
            vehicles_coords = []
            for veh in vehicles:
                x = veh.get_attribute('x')
                if x in vehicles_coords:
                    raise ValueError('На маршруте есть ТС с одинаковыми координатами.')
                    break
                vehicles_coords.append(x)

            if len(vehicles_coords) > 0:
                for veh in vehicles:
                    if veh.get_attribute("xmlns") is None:
                        action = ActionChains(self.driver)
                        action.double_click(veh).perform()
                        break
                break

        time.sleep(2)
        self.assertEqual(self.driver.current_url, "http://igs1.test.ruitb.ru/")
        print("test_go_to_map: PASSED")

    def test_switch_route(self):
        # Переход на ОО
        self.driver.find_element(*Main_menu_controls.TRAFFIC_ON_ROUTE_LINK).click()
        time.sleep(4)
        ch = ':'
        old_route_number = self.driver.find_element(*Route_situation.ROUTE_NUMBER).get_attribute('innerText')
        old_route_forward = self.driver.find_elements(*Route_situation.ROUTE_LEGEND)[0].get_attribute('innerText').split(ch, 1)[0]
        old_route_backward = self.driver.find_elements(*Route_situation.ROUTE_LEGEND)[2].get_attribute('innerText').split(ch, 1)[0]

        self.assertTrue(len(old_route_forward) > 0)
        self.assertTrue(len(old_route_backward) > 0)
        self.assertEqual(old_route_number, "60")

        input_field = self.driver.find_element(*Route_situation.ROUTE_NUMBER_INPUT)
        input_field.send_keys("23")
        route = self.driver.find_element(By.XPATH, "//*[starts-with(text(),'23')]")
        route.click()
        time.sleep(3)
        new_route_number = self.driver.find_element(*Route_situation.ROUTE_NUMBER).get_attribute('innerText')
        new_route_forward = self.driver.find_elements(*Route_situation.ROUTE_LEGEND)[0].get_attribute('innerText').split(ch, 1)[0]
        new_route_backward = self.driver.find_elements(*Route_situation.ROUTE_LEGEND)[2].get_attribute('innerText').split(ch, 1)[0]

        self.assertEqual(new_route_number, "23")
        self.assertTrue(len(new_route_forward) > 0)
        self.assertTrue(len(new_route_backward) > 0)
        self.assertNotEqual(old_route_forward, new_route_forward)
        self.assertNotEqual(old_route_backward, new_route_backward)
        print("test_switch_route: PASSED")

    def tearDown(self):
        self.driver.close()


if __name__ == "__main__":
    unittest.main()
