from selenium.webdriver.common.by import By


class Main_menu_controls(object):
    NAV_BUTTONS = (By.XPATH, "//div[@class='app-header-right']")
    MAP_LINK = (By.XPATH, "//a[@href='/']")
    TRAFFIC_ON_ROUTE_LINK = (By.XPATH, "//a[@href='/trafficonroute']")
    CHAT_LINK = (By.XPATH, "//a[@href='/incidentschat']")


class Create_message_form(object):
    MESSAGE_THEME = (By.ID, "incidentTitle")
    RECIEVER = (By.XPATH, "//div[contains(@class,'recipient-select')]//div//div//div//div//input")
    INFO_BTN = (By.XPATH, "//label[contains(@class,'ant-radio-button-wrapper info')]")
    WARN_BTN = (By.XPATH, "//label[contains(@class,'ant-radio-button-wrapper warn')]")
    URGENT_BTN = (By.XPATH, "//label[contains(@class,'ant-radio-button-wrapper urgent')]")
    MESSAGE_TEXT = (By.XPATH, "//textarea[@class='ant-input']")
    SEND_BTN = (By.XPATH, "//div[@class='app-incident-chat-input-right']//button")


class Route_situation(object):
    ROUTE_CONTAINERS = (By.XPATH, "//div[@class='VictoryContainer']")
    ROUTE_NUMBER = (By.XPATH, "//div[@class='routeSituationHeader']//div//div//span//div")
    ROUTE_NUMBER_INPUT = (By.XPATH, "//div[@class='routeSituationHeader']//div//div//span//input")
    ROUTE_LEGEND = (By.XPATH, "//div[@class='plotlegend']//div//div//div")
    CARD_TITLE = (By.XPATH, "//div[@class='ant-card-head-title']")


class Filter_elements:
    FOUND_ROUTES = (By.XPATH, "//div[@class='map-app-trafic-objects-object']")
    SEARCH_BTN = (By.XPATH, "//button[@class='ant-btn ant-btn-default ant-btn-icon-only map-app-tt-search-button']")
    VEHICLE_TYPE_BTNS = (By.XPATH, "//button[contains(@class,'vehicle-type-button')]")
