import sys

import selenium.common.exceptions
from selenium import webdriver
import time
import unittest
import warnings
from datetime import datetime

from selenium.webdriver import ActionChains, Keys
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
import re

from Elements import Main_menu_controls, Create_message_form, Route_situation


class Messages_tests(unittest.TestCase):

    # Запуск браузера
    def setUp(self):
        service = Service(r"C:\Program Files (x86)\chromedriver.exe")
        options = Options()
        options.binary_location = r"C:\Program Files\Google\Chrome\Application\chrome.exe"  # chrome binary location specified here
        options.add_argument("--start-maximized")  # open Browser in maximized mode
        options.add_argument("--no-sandbox")  # bypass OS security model
        options.add_argument("--disable-dev-shm-usage")  # overcome limited resource problems
        options.add_experimental_option('useAutomationExtension', False)
        warnings.simplefilter('ignore', category=ResourceWarning)

        self.driver = webdriver.Chrome(options=options, service=service)
        self.driver.get("http://igs1.test.ruitb.ru/")
        # self.driver.get("http://158.160.8.77:9999/")
        time.sleep(5)
        password_field = self.driver.find_element(By.XPATH, "//span[contains(@class, 'input')]//input")
        password_field.send_keys("admin")
        password_field.send_keys(Keys.ENTER)
        time.sleep(2)

    def test_write_message_to_singleDriver(self):
        # user_btn = self.driver.find_element(By.XPATH, "//div[@class='app-header-right']").find_elements(By.TAG_NAME, 'button')[3]
        # user_btn.click()
        # time.sleep(1)
        # dispatcher1 = self.driver.find_element(By.XPATH, "//div[@class='ant-space ant-space-vertical']").find_elements(By.TAG_NAME, 'input')[0]
        # dispatcher1.click()
        # time.sleep(2)

        mesage_btn = self.driver.find_element(*Main_menu_controls.NAV_BUTTONS).find_elements(By.TAG_NAME, 'button')[0]
        mesage_btn.click()
        time.sleep(1)

        now = datetime.now()
        current_time = now.strftime("%H:%M:%S")
        theme_text = "Селениум тест один водитель_" + current_time
        message_text = "Это сообщение было сгенерировано с помощью ПО Селениум"
        selected_vehicles = []

        theme = self.driver.find_element(*Create_message_form.MESSAGE_THEME)
        theme.send_keys(theme_text)
        time.sleep(1)

        self.driver.find_element(*Create_message_form.RECIEVER).send_keys("60")
        time.sleep(2)
        vehicles = self.driver.find_elements(By.XPATH, "//*[starts-with(text(),'8')]")
        for i in vehicles:
            idx = vehicles.index(i)
            if vehicles[idx].text != '':
                vehicles[idx].click()
                selected_vehicles.append(vehicles[idx].text)
                break

        theme.send_keys(Keys.ENTER)
        time.sleep(2)
        self.assertTrue(len(selected_vehicles) == 1)

        self.driver.find_element(*Create_message_form.INFO_BTN).click()
        time.sleep(1)
        self.driver.find_element(*Create_message_form.MESSAGE_TEXT).send_keys(message_text)
        time.sleep(2)
        self.driver.find_element(*Create_message_form.SEND_BTN).click()
        time.sleep(2)

        menu_links = self.driver.find_element(By.XPATH, "//div[@class='app-header']").find_elements(By.XPATH, './/li')
        # Переход в сообщения
        menu_links[2].click()
        time.sleep(10)

        mesage_list = self.driver.find_element(By.XPATH,
                                               "//div[@class='ant-collapse-item ant-collapse-item-active app-incident-list-group-info']").find_elements(
            By.TAG_NAME, "li")

        # Убираем из списка необработанные сообщения для проверки сортировки
        mesage_list_f = []
        for message in mesage_list:
            if len(message.find_elements(By.TAG_NAME, "button")) == 0:
                mesage_list_f.append(message)

        title_divs = mesage_list_f[0].find_elements(By.XPATH,
                                                    ".//div[@class='app-incident-list-item-details-title']//div")
        msg_divs = mesage_list_f[0].find_elements(By.XPATH,
                                                  ".//div[@class='app-incident-list-item-details-message']//div//div")

        title = title_divs[0].find_elements(By.XPATH, ".//span")[0].get_attribute('innerHTML')
        self.assertEqual(title, theme_text)

        reciver = msg_divs[0].get_attribute('innerHTML')
        self.assertTrue(selected_vehicles[0] in reciver)
        message = msg_divs[1].get_attribute('innerHTML')
        self.assertEqual(message, message_text)

        mesage_list_f[0].click()
        time.sleep(2)
        chat_title = self.driver.find_element(By.XPATH, "//div[@class='chat-header-group-title']").get_attribute(
            'innerHTML')
        chat_reciever = self.driver.find_element(By.XPATH,
                                                 "//div[@class='map-app-tt-select-route-item']").get_attribute(
            'innerText')
        chat_message = self.driver.find_element(By.XPATH,
                                                "//div[@class='chat-message-card-content-inner']").get_attribute(
            'innerHTML')
        self.assertEqual(title, chat_title)
        self.assertTrue(chat_reciever in reciver)
        self.assertEqual(message, chat_message)
        print("write_message_to_singleDriver: PASSED!")

    def test_write_message_to_infoGroup(self):

        mesage_btn = self.driver.find_element(*Main_menu_controls.NAV_BUTTONS).find_elements(By.TAG_NAME, 'button')[0]
        mesage_btn.click()
        time.sleep(1)

        now = datetime.now()
        current_time = now.strftime("%H:%M:%S")
        theme_text = "Селениум тест группа_" + current_time
        message_text = "Это сообщение было сгенерировано с помощью ПО Селениум"
        selected_vehicles = []

        theme = self.driver.find_element(*Create_message_form.MESSAGE_THEME)
        theme.send_keys(theme_text)
        time.sleep(1)
        self.driver.find_element(*Create_message_form.RECIEVER).send_keys("60")
        time.sleep(2)
        vehicles = self.driver.find_elements(By.XPATH, "//*[starts-with(text(),'8')]")
        for i in vehicles:
            idx = vehicles.index(i)
            if vehicles[idx].text != '':
                vehicles[idx].click()
                selected_vehicles.append(vehicles[idx].text)
                time.sleep(0.5)
        try:
            self.assertTrue(len(vehicles) >= 2)
        except AssertionError:
            print("Недостаточно ТС для отправки группе")
            raise AssertionError

        theme.send_keys(Keys.ENTER)
        time.sleep(1)

        self.driver.find_element(*Create_message_form.INFO_BTN).click()
        self.driver.find_element(*Create_message_form.MESSAGE_TEXT).send_keys(message_text)
        time.sleep(1)
        self.driver.find_element(*Create_message_form.SEND_BTN).click()
        time.sleep(2)

        menu_links = self.driver.find_element(By.XPATH, "//div[@class='app-header']").find_elements(By.XPATH, './/li')
        # Переход в сообщения
        menu_links[2].click()
        time.sleep(12)

        infogroup = self.driver.find_element(By.XPATH,
                                             "//div[@class='ant-collapse-item ant-collapse-item-active app-incident-list-group-info']")
        mesage_list = infogroup.find_elements(By.TAG_NAME, "li")

        # Убираем из списка необработанные сообщения для проверки сортировки
        mesage_list_f = []
        for message in mesage_list:
            if len(message.find_elements(By.TAG_NAME, "button")) == 0:
                mesage_list_f.append(message)

        title_divs = mesage_list_f[0].find_elements(By.XPATH,
                                                    ".//div[@class='app-incident-list-item-details-title']//div")

        title = title_divs[0].find_elements(By.XPATH, ".//span")[0].get_attribute('innerHTML')
        self.assertEqual(title, theme_text)

        for vNumber in selected_vehicles:
            try:
                reciever = mesage_list_f[0].find_element(By.XPATH, ".//*[contains(text(),'" + vNumber + "')]")
                status = reciever.find_element(By.XPATH, "..//span[@class='is-sent']")
            except NoSuchElementException:
                print("Сообщение не было доставлено ТС ", vNumber)
                raise NoSuchElementException
            message = status.find_element(By.XPATH, ".//span").text
            self.assertEqual(message, message_text)

        print("write_message_to_infoGroup: PASSED")

    def test_write_message_from_route_situation(self):
        route_has_vehicles = False
        max_recievers = 5
        self.driver.find_element(*Main_menu_controls.TRAFFIC_ON_ROUTE_LINK).click()
        time.sleep(5)

        mesage_btn = self.driver.find_element(*Main_menu_controls.NAV_BUTTONS).find_elements(By.TAG_NAME, 'button')[0]
        mesage_btn.click()
        time.sleep(1)
        selected_vehicles = []
        routeContainers = self.driver.find_elements(*Route_situation.ROUTE_CONTAINERS)

        for container in routeContainers:
            index = 0
            vehicles = container.find_elements(By.XPATH, "./*")[0].find_elements(By.TAG_NAME, "svg")
            vehicles_titles = container.find_elements(By.XPATH, "./*")[0].find_elements(By.XPATH,
                                                                                        ".//*[contains(@id, 'scatterVehicles-labels')]")
            for veh in vehicles:
                if len(selected_vehicles) < max_recievers:
                    try:
                        if veh.get_attribute("xmlns") is None:
                            route_has_vehicles = True
                            veh_number = vehicles_titles[index].find_elements(By.XPATH, "./*")[1].get_attribute(
                                'innerHTML')
                            veh.click()
                            time.sleep(0.2)
                            # try:
                            #     s = self.driver.find_element(By.XPATH, "//div[@class='ant-notification ant-notification-top']")
                            #
                            # except NoSuchElementException:
                            selected_vehicles.append(veh_number)
                            recievers = self.driver.find_element(*Create_message_form.RECIEVER).find_elements(By.XPATH, "//div[@class ='map-app-tt-select-route-item']")
                            try:
                                self.assertEqual(len(recievers)-1, len(selected_vehicles))
                            except AssertionError:
                                try:
                                    self.driver.find_element(By.XPATH, "//div[@class='ant-notification ant-notification-top']")
                                    selected_vehicles.remove(veh_number)
                                except NoSuchElementException:
                                    raise NoSuchElementException
                        index += 1
                    except selenium.common.exceptions.StaleElementReferenceException:
                        print("ТС вне графа: ")

        try:
            self.assertEqual(route_has_vehicles, True)
        except AssertionError:
            print("Нет данных для проверки")
            raise AssertionError

        now = datetime.now()
        current_time = now.strftime("%H:%M:%S")
        theme_text = "Селениум тест ОО_" + current_time
        theme = self.driver.find_element(*Create_message_form.MESSAGE_THEME)
        theme.send_keys(theme_text)
        time.sleep(1)

        message_text = "Это сообщение было сгенерировано с помощью ПО Селениум"
        self.driver.find_element(*Create_message_form.INFO_BTN).click()
        self.driver.find_element(*Create_message_form.MESSAGE_TEXT).send_keys(message_text)
        time.sleep(1)
        self.driver.find_element(*Create_message_form.SEND_BTN).click()
        time.sleep(2)

        menu_links = self.driver.find_element(By.XPATH, "//div[@class='app-header']").find_elements(By.XPATH, './/li')
        # Переход в сообщения
        menu_links[2].click()
        time.sleep(12)

        infogroup = self.driver.find_element(By.XPATH,
                                             "//div[@class='ant-collapse-item ant-collapse-item-active app-incident-list-group-info']")
        mesage_list = infogroup.find_elements(By.TAG_NAME, "li")

        # Убираем из списка необработанные сообщения для проверки сортировки
        mesage_list_f = []
        for message in mesage_list:
            if len(message.find_elements(By.TAG_NAME, "button")) == 0:
                mesage_list_f.append(message)

        title_divs = mesage_list_f[0].find_elements(By.XPATH,
                                                    ".//div[@class='app-incident-list-item-details-title']//div")

        title = title_divs[0].find_elements(By.XPATH, ".//span")[0].get_attribute('innerHTML')
        self.assertEqual(title, theme_text)

        for vNumber in selected_vehicles:
            try:
                reciever = mesage_list_f[0].find_element(By.XPATH, ".//*[contains(text(),'" + vNumber + "')]")
                status = reciever.find_element(By.XPATH, "..//span[@class='is-sent']")
            except NoSuchElementException:
                print("Сообщение не было доставлено ТС ", vNumber)
                raise NoSuchElementException
            message = status.find_element(By.XPATH, ".//span").text
            self.assertEqual(message, message_text)

        print("test_write_message_from_route_situation: PASSED")

    def tearDown(self):
        self.driver.close()


if __name__ == "__main__":
    unittest.main()
