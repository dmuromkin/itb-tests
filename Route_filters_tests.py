import sys

from selenium import webdriver
import time
import unittest
import warnings
from datetime import datetime

from selenium.webdriver import ActionChains, Keys
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
import re

from Elements import Main_menu_controls, Create_message_form, Filter_elements


class Route_filters_tests(unittest.TestCase):

    route_to_select = ['60', '24', '23', '39']

    # Запуск браузера
    def setUp(self):
        service = Service(r"C:\Program Files (x86)\chromedriver.exe")
        options = Options()
        options.binary_location = r"C:\Program Files\Google\Chrome\Application\chrome.exe"  # chrome binary location specified here
        options.add_argument("--start-maximized")  # open Browser in maximized mode
        options.add_argument("--no-sandbox")  # bypass OS security model
        options.add_argument("--disable-dev-shm-usage")  # overcome limited resource problems
        options.add_experimental_option('useAutomationExtension', False)
        warnings.simplefilter('ignore', category=ResourceWarning)

        self.driver = webdriver.Chrome(options=options, service=service)
        self.driver.get("http://igs1.test.ruitb.ru/")
        # self.driver.get("http://158.160.8.77:9999/")
        time.sleep(5)
        password_field = self.driver.find_element(By.XPATH, "//span[contains(@class, 'input')]//input")
        password_field.send_keys("admin")
        password_field.send_keys(Keys.ENTER)
        time.sleep(4)

    def select_missing_routes(self):

        user_btn = self.driver.find_element(*Main_menu_controls.NAV_BUTTONS).find_elements(By.TAG_NAME, 'button')[3]
        user_btn.click()
        time.sleep(1)
        rote_settings = self.driver.find_element(By.XPATH, "//*[text()='Настройки маршрутов']")
        rote_settings.click()
        time.sleep(1)
        route_list = self.driver.find_element(By.XPATH, "//div[@class='ant-modal-content']").find_elements(By.TAG_NAME, 'li')

        for r in route_list:
            route_text = r.find_element(By.XPATH, ".//span[@class='map-app-tt-ptoperty-value']").get_attribute('innerText')
            if route_text in Filters_tests.route_to_select:
                try:
                    checbox = r.find_element(By.XPATH, ".//span[@class='ant-checkbox']")
                    checbox.click()
                except NoSuchElementException:
                    print('Уже выбрано')
                time.sleep(1)
        chosen = self.driver.find_element(By.XPATH, "//*[contains(text(),'Выбрано:')]").get_attribute('innerText')
        self.assertEqual(chosen, f"Выбрано: {len(Route_filters_tests.route_to_select)}")
        choose_btn = self.driver.find_element(By.XPATH, "//button[@class='ant-btn ant-btn-primary map-modal-button']")
        choose_btn.click()
        time.sleep(2)
        self.driver.find_element(*Filter_elements.SEARCH_BTN).click()
        time.sleep(3)

    def test_typeTS_route_filter(self):
        found_routes = self.driver.find_elements(*Filter_elements.FOUND_ROUTES)
        try:
            self.assertGreater(len(found_routes), len(Route_filters_tests.route_to_select)-1)
        except AssertionError:
            Filters_tests.select_missing_routes(self)
            found_routes = self.driver.find_elements(*Filter_elements.FOUND_ROUTES)
            self.assertGreater(len(found_routes),  len(Route_filters_tests.route_to_select)-1)

        tram_type_btn = self.driver.find_elements(*Filter_elements.VEHICLE_TYPE_BTNS)[0]
        icon_color = tram_type_btn.find_element(By.TAG_NAME, 'path').get_attribute("fill")
        icon_picture = tram_type_btn.find_element(By.TAG_NAME, 'path').get_attribute("d")
        trolley_type_btn = self.driver.find_elements(*Filter_elements.VEHICLE_TYPE_BTNS)[1]
        trolley_type_btn.click()
        time.sleep(1)

        self.driver.find_element(*Filter_elements.SEARCH_BTN).click()
        time.sleep(1)

        filtered_elements = self.driver.find_elements(*Filter_elements.FOUND_ROUTES)
        for filtered_element in filtered_elements:
            filtered_icon = filtered_element.find_element(By.TAG_NAME, 'path')
            self.assertEqual(icon_picture, filtered_icon.get_attribute("d"))
            self.assertEqual(icon_color, filtered_icon.get_attribute("fill"))
        print("\ntest_typeTS_route_filter: PASSED")

    # Проверка работы фильтра по номеру маршрута
    def test_routeNmber_route_filter(self):
        found_routes = self.driver.find_elements(*Filter_elements.FOUND_ROUTES)
        try:
            self.assertGreater(len(found_routes), len(Route_filters_tests.route_to_select)-1)
        except AssertionError:
            Filters_tests.select_missing_routes(self)
            found_routes = self.driver.find_elements(*Filter_elements.FOUND_ROUTES)
            self.assertGreater(len(found_routes), len(Route_filters_tests.route_to_select)-1)
        total_routes_span = self.driver.find_element(By.XPATH,
                                                     "//div[@class='ui-kit-search-result-total-quantity']//span")
        trolley_type_btn = self.driver.find_elements(*Filter_elements.VEHICLE_TYPE_BTNS)[1]
        trolley_type_btn.click()
        time.sleep(2)
        self.driver.find_element(*Filter_elements.SEARCH_BTN).click()
        time.sleep(1)
        filtered_elements = self.driver.find_elements(*Filter_elements.FOUND_ROUTES)
        search_field = self.driver.find_elements(By.XPATH, "//input[@class='ant-select-selection-search-input']")[0]
        routes_to_check = []
        expected_count = 0
        if len(filtered_elements) > 2:
            routes_to_check.append(filtered_elements[0])
            routes_to_check.append(filtered_elements[-1])
            expected_count = 2
        else:
            self.driver.close()

        for r in routes_to_check:
            r_number = r.find_element(By.XPATH, ".//span[@class='map-app-tt-ptoperty-value']").get_attribute(
                'innerHTML')
            search_field.send_keys(r_number)
            time.sleep(2)
            search_field.send_keys(Keys.ENTER)

        self.driver.find_element(*Filter_elements.SEARCH_BTN).click()
        time.sleep(2)
        current_count = re.findall(r'\b\d+\b', total_routes_span.get_attribute('innerHTML'))[0]
        self.assertEqual(current_count, str(expected_count))
        filtered_elements = self.driver.find_elements(*Filter_elements.FOUND_ROUTES)
        self.assertEqual(len(filtered_elements), expected_count)
        print("\ntest_routeNmber_route_filter: PASSED")

    # Проверка фильтра по названию отсановки
    def test_station_route_filter(self):
        found_routes = self.driver.find_elements(*Filter_elements.FOUND_ROUTES)
        try:
            self.assertGreater(len(found_routes),  len(Route_filters_tests.route_to_select)-1)
        except AssertionError:
            Filters_tests.select_missing_routes(self)
            found_routes = self.driver.find_elements(*Filter_elements.FOUND_ROUTES)
            self.assertGreater(len(found_routes),  len(Route_filters_tests.route_to_select)-1)
        search_station_field = self.driver.find_element(By.XPATH, "//input[@class='ant-input']")
        search_station_field.send_keys("Северная")
        time.sleep(1)
        search_station_field.send_keys(Keys.ENTER)
        time.sleep(1)
        filtered_elements = self.driver.find_elements(By.XPATH, "//div[@class='map-app-trafic-objects-object']")
        self.assertEqual(len(filtered_elements), 1)
        route_name = filtered_elements[0].find_element(By.XPATH,
                                                       ".//span[@class='map-app-tt-ptoperty-value']").get_attribute(
            'innerHTML')
        self.assertEqual(route_name, '60')
        print("\ntest_station_route_filter: PASSED")

    def tearDown(self):
        self.driver.close()


if __name__ == "__main__":
    unittest.main()
